
# Task 1 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

# my_list = [3, 8, 'good', True, (2, 8), {'Oct': 10}]
#
# for i in my_list:
#     print(type(i))

# Task 2 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

input_list = input('Введите список слов или цифр через запятую без пробелов: ')

my_list = input_list.split(",")

# i = 0
# first = 0
# last = 0
# for el in my_list:
#     if (i % 2) == 0:
#         first = el
#     else:
#         my_list[i] = first
#         my_list[i-1] = el
#     i += 1
# print(my_list)

## Task 2 TEACHER  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

# for index in range(0, len(my_list)-1, 2):
#     my_list[index], my_list[index+1] = my_list[index+1], my_list[index]

# Task 3.1 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
#
# try:
#     month_in = int(input('Введите номер месяца от 1 до 12: '))
#
# except ValueError:
#     print('Вы ввели не число')
#     exit(1)
#
# months_dict = {'зима': (12, 1, 2), 'весна': (3, 4, 5), 'лето': (6, 7, 8), 'осень': (9, 10, 11)}
#
# if month_in >= 1 and month_in <= 12:
#     for key, values in months_dict.items():
#         if month_in in values:
#             print(f'{month_in}-й месяц - {key}')
# else:
#     print('Такого месяца нет на планете Земля!')
#
# Task 3.2 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
#
# try:
#     month_in = int(input('Введите номер месяца от 1 до 12: '))
#
# except ValueError:
#     print('Вы ввели не число')
#     exit(1)
#
# winter = [12, 1, 2]
# spring = [3, 4, 5]
# summer = [6, 7, 8]
# autumn = [6, 7, 8]
#
# if month_in >= 1 and month_in <= 12:
#     if month_in in winter:
#         print(f'{month_in}-й месяц - зима')
#     elif month_in in spring:
#         print(f'{month_in}-й месяц - весна')
#     elif month_in in summer:
#         print(f'{month_in}-й месяц - лето')
#     else:
#         print(f'{month_in}-й месяц - осень')
# else:
#     print('Такого месяца нет на планете Земля!')
#
# Task 4 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

# input_list = input('Введите строку из нескольких слов через пробелы: ')
#
# my_list = input_list.split(" ")
# n = 0
#
# for v in my_list:
#     letters = len(v)
#     if letters < 10:
#         print(f'{n}. {v}')
#     else:
#         print(f'{n}. {v[:10]}')
#     n +=1

# Task 5 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
#
# try:
#     num_in = int(input('Введите целое натуральное число: '))
# except ValueError:
#     print('Некорректный ввод')
#     exit(1)
#
# my_list = [8, 4, 4, 3, 2]
# found = False
#
#
# if num_in < 1:
#     print('вы ввели отрицательное число!')
#     num_in = int(input('Введите целое натуральное число: '))
# else:
#     for num, el in enumerate(my_list):
#         if el <= num_in:
#             my_list.insert(num, num_in)
#             found = True
#             break
#     if not found:
#         my_list.append(num_in)
#
# print(my_list)

# # Task 6 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
#
# goods = [
#     (1, {"название": "компьютер", "цена": 20000, "количество": 5, "eд": "шт."}),
#     (2, {"название": "принтер", "цена": 6000, "количество": 2, "eд": "шт."}),
#     (3, {"название": "сканер", "цена": 2000, "количество": 7, "eд": "шт."})
# ]
#
# i = len(goods) + 1
#
# while True:
#     name = input("Введите название товара: ")
#     price = input("Введите цену товара: ")
#     count = input("Введите количество товара: ")
#     unit = input("Введите единицу товара: ")
#     goods.append((i, {"название": name, "цена": price, "количество": count, "eд": unit}))
#     end = input("Хватит? ").lower()
#     i += 1
#     if end == 'yes' or end == 'да':
#         break;
#
# print(goods)
#
# analytic = {}
# keys = []
# names = []
# prices = []
# counts = []
# units = []
#
# for good in goods:
#     good = good[1]
#     for key, val in good.items():
#         if key == 'название':
#             names.append(val)
#         elif key == 'цена':
#             prices.append(val)
#         elif key == 'количество':
#             counts.append(val)
#         elif key == 'eд':
#             units.append(val)
#
# analytic = {
#     "название": names,
#     "цена": prices,
#     "counts": counts,
#     "units": units,
# }
#
# print(analytic)