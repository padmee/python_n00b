
## Task 1 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
#
# from sys import argv
#
#
# def salary(hours, rate, bonus):
#     return float(hours)*float(rate)+float(bonus)
#
# script_name,hours, rate, bonus = argv
#
# print(salary(hours, rate, bonus))

## Task 2 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

# input_list = [300, 2, 12, 58, 44, 1, 1, 4, 10, 7, 1, 78, 123, 55]
#
# result_list = [el for el_index, el in enumerate(input_list[1:],1) if el > input_list[el_index-1]]
#
# print(result_list)

## Task 3 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

# result_list = [el for el in range(20,240) if el%20==0 or el%21==0]
# print(result_list)

## Task 4 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

# input_list = [2, 2, 2, 7, 23, 1, 44, 44, 3, 2, 10, 7, 4, 11]
#
# result_list = [el for el in input_list if input_list.count(el)==1 ]
# print(result_list)

## Task 5 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
#
# from functools import reduce
#
# result_list = [el for el in range(100, 1001) if el%2==0]
# print(result_list)
# print(reduce(lambda x, y: x*y, result_list))

## Task 6 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
#
import itertools
# start_num = int(input('Введите целое число: '))
# for i in itertools.count(start_num, 1):
#     if i > start_num*10:
#         break
#     print(i)
#
# input_list = [2, 2, 5, 98, 2, 7, 23, 1, 44, 44, 3, 2, 10, 7, 4, 11]
# count_el = 0
# for i in itertools.cycle(input_list):
#     if count_el  == len(input_list)*2:
#         break
#     count_el +=1
#     print(i)

## Task 7 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

#
# def fact(n):
#     r = 1
#     for el in range(1, n + 1):
#         r *= el
#         yield r
#
#
# new_list = [el for el in fact(7)]
# print(new_list)