# Task 1 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
#
# def func_1(a,b):
#     """
#     Function divides first number by second
#     """
#     if b == 0:
#         print('Делить на ноль нельзя!')
#         return
#     else:
#         c = a / b
#
#     return c
#
# try:
#     num_1 = float(input('Введите первое число: '))
#     num_2 = float(input('Введите второе число: '))
# except ValueError:
#     print('Вы ввели не число')
#     exit(1)
#
# print(func_1(num_1, num_2))

#Task 2 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
#
# def func_2(**kwargs):
#     """
#     Function divides first number by second
#     """
#     name = kwargs['name']
#     surname = kwargs['surname']
#     year = kwargs['year']
#     town = kwargs['town']
#     email = kwargs['email']
#     phone = kwargs['phone']
#
#     print(f'{name} {surname}, {year} года рождения, из города {town}, email: {email}, телефон: {phone}')
#
# func_2(name = input("Введите ваше имя: "),
#        surname = input("Введите вашу фамилию: "),
#        year = input("Введите год вашего рождения: "),
#        town = input("Введите город вашего проживания: "),
#        email = input("Введите ваш email: "),
#        phone = input("Введите ваш телефон: ")
#        )

##Task 3 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
#
# def my_func(a,b,c):
#     my_list = [a, b, c]
#     my_list.sort(reverse=True)
#     result = sum(my_list[:2])
#     return result
#
# print(my_func(16, 3, 1))

## Task 4 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
#
# def my_func_1(x, y):
#     z = x**y
#     return z
#
# def my_func_2(x: int, y: int) -> float:
#     if y > 0:
#         return
#     if y == 0:
#         return 1
#     else:
#         ##return 1/x*my_func_2(x,y+1)

        # n = 1
        # z = 1/x
        # while n < abs(y):
        #     z *= 1/x
        #     n += 1
        # return z

# print(my_func_1(500,-3))
# print(my_func_2(500,-3))

# Task 5 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
#
result = 0
#
# def my_func_3(args):
#     global result
#     for el in args:
#         result += el
#     return result
#
# exit_bool = False
#
# while exit_bool is False:
#     input_list = [int(x) for x in (input('Введите несколько цифр через пробелы: ')).split(" ")]
#     print(f'Сума значений {my_func_3(input_list)}')
#     input_exit = input('Продолжить? Введите: "да" или "нет" ')
#
#     if input_exit == 'нет':
#         exit_bool = True
#     else:
#         continue

# # Task 6 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

## def int_func(txt):
##     return txt.title()
##
## print(int_func('text'))

# int_func = lambda txt: txt.title()
#
# input_string = 'this is my text'
# string_list = input_string.split()
# new_list = []
# for el in string_list:
#     new_list.append(int_func(el))
#
# print(*new_list)
#
# print(" ".join(new_list))